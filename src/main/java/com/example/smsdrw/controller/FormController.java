package com.example.smsdrw.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.smsdrw.model.Form;

@RestController
@RequestMapping("/form")
public class FormController extends controller<Form>  {

	@Override
	public ResponseEntity<?> list() {
		// TODO Auto-generated method stub
		return ResponseEntity.ok(service.form.list());
	}

	@Override
	public ResponseEntity<?> post(Form data) {
		// TODO Auto-generated method stub
		return ResponseEntity.ok(service.form.post(data));
	}

	@Override
	public ResponseEntity<?> get(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
