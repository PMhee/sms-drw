package com.example.smsdrw.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.smsdrw.model.SmsAccount;

@RestController
@RequestMapping("/smsAccount")
public class SmsAccountController extends controller<SmsAccount> {

	@Override
	public ResponseEntity<?> get(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<?> list() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<?> post(SmsAccount data) {
		// TODO Auto-generated method stub
		return ResponseEntity.ok(service.smsAccount.post(data));
	}

}
