package com.example.smsdrw.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.smsdrw.model.Agreement;
@RestController
@RequestMapping("/agreement")
public class AgreementController extends controller<Agreement> {

	@Override
	public ResponseEntity<?> list() {
		// TODO Auto-generated method stub
		return ResponseEntity.ok(service.agreement.list());
	}

	@Override
	public ResponseEntity<?> post(@RequestBody Agreement data) {
		// TODO Auto-generated method stub
		return ResponseEntity.ok(service.agreement.post(data));
	}

	@Override
	public ResponseEntity<?> get(@RequestParam Long id) {
		// TODO Auto-generated method stub
		return ResponseEntity.ok(service.agreement.get(id));
	}

}
