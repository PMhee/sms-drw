package com.example.smsdrw.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.smsdrw.model.Form;


@Repository
public interface FormRepository extends JpaRepository<Form, Long> {
}

