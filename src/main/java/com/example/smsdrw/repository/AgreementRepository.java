package com.example.smsdrw.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.smsdrw.model.Agreement;
@Repository
public interface AgreementRepository extends JpaRepository<Agreement,Long> {
}
