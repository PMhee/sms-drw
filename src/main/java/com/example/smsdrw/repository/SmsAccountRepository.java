package com.example.smsdrw.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.smsdrw.model.SmsAccount;

@Repository
public interface SmsAccountRepository extends JpaRepository<SmsAccount,Long> {

}
