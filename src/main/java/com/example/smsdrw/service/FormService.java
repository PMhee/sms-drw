package com.example.smsdrw.service;

import org.springframework.stereotype.Service;

import com.example.smsdrw.model.Form;
import com.example.smsdrw.repository.FormRepository;

@Service
public class FormService extends BaseService<Form,FormRepository> {
}
