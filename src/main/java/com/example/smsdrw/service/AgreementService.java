package com.example.smsdrw.service;

import org.springframework.stereotype.Service;

import com.example.smsdrw.model.Agreement;
import com.example.smsdrw.repository.AgreementRepository;

@Service
public class AgreementService extends BaseService<Agreement,AgreementRepository> {
}
