package com.example.smsdrw.service;

import org.springframework.stereotype.Service;

import com.example.smsdrw.model.SmsAccount;
import com.example.smsdrw.repository.SmsAccountRepository;

@Service
public class SmsAccountService extends BaseService<SmsAccount,SmsAccountRepository> {

}
